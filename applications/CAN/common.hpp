/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-03-13     白给王       the first version
 */
#ifndef APPLICATIONS_CAN_COMMON_HPP_
#define APPLICATIONS_CAN_COMMON_HPP_

#include <rtthread.h>
#include "rtdevice.h"
#include "stdlib.h"
#include "string.h"
#include "pinDefinitions.hpp"

rt_bool_t can_send(struct rt_can_msg msg); //发送CAN命令

#endif /* APPLICATIONS_CAN_COMMON_HPP_ */
