/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-03-13     白给王       the first version
 */
#ifndef APPLICATIONS_PINDEFINITIONS_HPP_
#define APPLICATIONS_PINDEFINITIONS_HPP_

#include <rtthread.h>
#include "rtdevice.h"
#include "board.h"

#define CAN_DEV_NAME        "can1"

#endif /* APPLICATIONS_PINDEFINITIONS_HPP_ */
